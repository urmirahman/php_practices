<?php

class Database
{
    public $host = "localhost";
    public $database = "dummyps";
    public $username = "root";
    public $pass = "";
    public $conn = "";

    public function getconnection()
    {

        $this->conn = new mysqli($this->host, $this->username, $this->pass, $this->database);

        if ($this->conn) {

            // echo json_encode("connection successfull");
        } else {
            // echo json_encode("connection failed");
        }

        return $this->conn;
    }
}
