<?php
include_once "./controller.php";
include_once "../model/database.php";

$conn = new Database();
$db = $conn->getconnection();
$project = new Project($db);

$data = json_decode(file_get_contents("php://input"));

if (!empty($data)) {
    $project->updatePM($data->pm_id, $data->name, $data->project_id);
}
