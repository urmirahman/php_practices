<?php
include_once "../model/database.php";

class Project
{
    public $db;

    public function __construct($db)
    {
        $this->db = $db;

    }
    ////project table

    public function readAllProject()
    {
        $query = "SELECT * FROM projects";
        $result = mysqli_query($this->db, $query);

        return $result;

    }

    public function insertProject($name, $project_status)
    {
        $query = "INSERT INTO projects(name,project_status) VALUES('$name','$project_status')";
        if (mysqli_query($this->db, $query)) {
            echo json_encode("inserted");
            http_response_code(201);
        } else {
            http_response_code(500);
            echo json_encode("unable to insert");
        }
    }

    public function updateProject($id, $name, $project_status)
    {

        $query = "UPDATE projects SET name='$name',project_status='$project_status' WHERE project_id=$id";

        if (mysqli_query($this->db, $query)) {
            http_response_code(201);
            echo json_encode("project updated");
        } else {
            http_response_code(501);
            echo json_encode("unable to update project");
        }
    }

    public function deleteproject($id)
    {
        $query = "DELETE FROM projects WHERE project_id=$id";
        if (mysqli_query($this->db, $query)) {
            http_response_code(201);
            echo json_encode("project deleted");
        } else {
            http_response_code(501);
            echo json_encode("unable to delete project");
        }
    }

    ///project manager table

    public function insertPM($name, $project_id)
    {
        $query = "INSERT INTO project_manager(name,project_id) VALUES('$name','$project_id')";
        if (mysqli_query($this->db, $query)) {
            echo json_encode("inserted");
            http_response_code(201);
        } else {
            http_response_code(500);
            echo json_encode("unable to insert");
        }
    }

    public function readAllPM()
    {
        $query = "SELECT * FROM project_manager";
        $result = mysqli_query($this->db, $query);

        return $result;

    }

    public function updatePM($id, $name, $project_id)
    {

        $query = "UPDATE project_manager SET name='$name',project_id='$project_id' WHERE project_manager.pm_id='$id'";

        if (mysqli_query($this->db, $query)) {
            http_response_code(201);
            echo json_encode(" updated");
        } else {
            http_response_code(501);
            echo json_encode("unable to update ");
        }
    }

    public function deletePM($id)
    {
        $query = "DELETE FROM project_manager WHERE pm_id=$id";
        if (mysqli_query($this->db, $query)) {
            http_response_code(201);
            echo json_encode(" deleted");
        } else {
            http_response_code(501);
            echo json_encode("unable to delete ");
        }
    }

}
