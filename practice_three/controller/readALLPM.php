<?php
include_once "./controller.php";
include_once "../model/database.php";

$conn = new Database();
$db = $conn->getconnection();
$project = new Project($db);

$data = $project->readAllPM();

$p = array();

if ($data->num_rows > 0) {
    while ($row = $data->fetch_assoc()) {
        $p[] = $row;
    }
    http_response_code(200);
    echo json_encode($p);

} else {
    http_response_code(404);
    echo json_encode("0 data found");
}
