<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>edit</title>
  </head>
  <body>

  <div class="container justify-content-center mt-3 ">
  <div class="form">
    <form method= "post" action="../controller/project_controller.php">
    <div class="form-group">
    <label for="projectid">project Id</label>
    <input type="text" name= "projectid" class="form-control" id="projectid"  placeholder="project id">
  </div>
  <div class="form-group">
    <label for="projectname">project Name</label>
    <input type="text" name= "projectname" class="form-control" id="projectname"  placeholder="project name">
  </div>
  <div class="form-group">
    <label for="projectmanager">Project Manager</label>
    <input type="text" name="projectmanager" class="form-control" id="projectmanager" placeholder="project manager">
  </div>
  <div class="form-group">
  <label for="projectStatus">project Status</label>

    <input type="text" name="projectStatus" class="form-control" id="projectStatus" placeholder="project status">
  </div>
  <button type="submit" name="edit" class="btn btn-primary">Update</button>
  <button type="submit" name="del" class="btn btn-primary">Delete</button>

</form>
    </table>
    </div>

  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
