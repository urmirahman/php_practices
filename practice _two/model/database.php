
<?php
class Database
{
    public $host = "localhost";
    public $username = "root";
    public $database = "demoapi";
    public $pass = "";
    public $conn;

    public function getconnection()
    {
        //host -> user ->pass
        $this->conn = new mysqli($this->host, $this->username, $this->pass, $this->database);

        if ($this->conn) {
            //echo json_encode("connection successfull") . "<br>";
        } else {
            echo "failed";
        }

        return $this->conn;
    }

}

?>