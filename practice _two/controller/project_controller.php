<?php
include_once "../model/database.php";

// include database object
$db = new Database();
$conn = $db->getconnection();
// get project fields
if (isset($_POST['insert'])) {
    $p_name = htmlspecialchars($_POST['projectname']);
    $p_manager = htmlspecialchars($_POST['projectmanager']);
    $p_status = htmlspecialchars($_POST['projectStatus']);

} elseif (isset($_POST['edit']) || isset($_POST['del'])) {
    $p_name = htmlspecialchars($_POST['projectname']);
    $p_manager = htmlspecialchars($_POST['projectmanager']);
    $p_status = htmlspecialchars($_POST['projectStatus']);
    $p_id = htmlspecialchars($_POST['projectid']);
}

//check CRUD  operation
if (isset($_POST['insert'])) {
    insert_project($p_name, $p_manager, $p_status, $conn);
} elseif (isset($_POST['update'])) {
    header("Location:../view/edit.php");
} elseif (isset($_POST['edit'])) {
    update_project($p_id, $p_name, $p_manager, $p_status, $conn);
} elseif (isset($_POST['delete'])) {
    header("Location:../view/edit.php");
} elseif (isset($_POST['del'])) {
    delete_project($p_id, $conn);
}

// insert into database
function insert_project($name, $manager, $status, $conn)
{
    $query = "INSERT INTO projects(project_name,project_manager,project_status) VALUES('$name','$manager','$status')";
    if (mysqli_query($conn, $query)) {
        http_response_code(201);
        echo json_encode("Project created");

    } else {
        http_response_code(500);
        echo json_encode("unable to create project");
    }
}

//update into database
function update_project($id, $name, $manager, $status, $conn)
{
    $query = "UPDATE projects SET project_name='$name', project_manager='$manager', project_status='$status' WHERE id=$id";
    if (mysqli_query($conn, $query)) {
        http_response_code(200);
        echo json_encode("Project updated");

    } else {
        http_response_code(500);
        echo json_encode("Unable to update project");
    }
}

//delete database
function delete_project($id, $conn)
{
    $query = "DELETE FROM projects WHERE id =$id";
    if (mysqli_query($conn, $query)) {
        echo json_encode("project deleted");

    } else {
        echo json_encode("failed to delete");
    }
}

//get all project list
function showall_project($conn)
{
    $data = array();
    $query = "SELECT * FROM projects ";
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;

        }
        http_response_code(200);
    } else {
        json_encode("O Data found");
    }
    return $data;
}

function select_one($id, $conn)
{
    $data = array();
    $query = "SELECT * FROM  projects WHERE id=$id";
    $result = mysqli_query($conn, $query); // do not write mysql_connect instead of  mysqli_query :'( i did that mistake and stuck for an hour )
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
        http_response_code(200);
    } else {
        json_encode("O Data found");
    }
    return $data;

}
