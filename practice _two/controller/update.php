<?php
include_once "./project_controller.php";

$data = json_decode(file_get_contents("php://input"));

if (!empty($data->id) &&
    !empty($data->project_name) &&
    !empty($data->project_manager) &&
    !empty($data->project_status)
) {
    update_project($data->id, $data->project_name, $data->project_manager, $data->project_status, $conn);
} else {
    json_encode("request body is empty");
}
