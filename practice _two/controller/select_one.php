<?php

include_once "./project_controller.php";
if (isset($_GET['id'])) {

    $data = $_GET['id'];
    //echo json_decode($data);

    $record = select_one($data, $conn);
    if (!empty($record)) {
        echo json_encode($record);
    } else {
        json_encode("not found");
    }
}
