<?php
include_once "./project_controller.php";

$records = showall_project($conn);

if (!empty($records)) {
    http_response_code(200);
    echo json_encode($records);
} else {
    http_response_code(404);
    echo json_encode("No data found in Database");
}
