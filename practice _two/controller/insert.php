<?php
include_once "./project_controller.php";

$data = json_decode(file_get_contents("php://input"));

if (!empty($data->project_name) &&
    !empty($data->project_manager) &&
    !empty($data->project_status)) {
    insert_project($data->project_name, $data->project_manager, $data->project_status, $conn);
} else {
    json_encode("request body is empty");
}
